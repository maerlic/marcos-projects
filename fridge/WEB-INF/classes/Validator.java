/*Name: Marco Aerlic
  Student ID: 20690053
  Username: 20690053@students.ltu.edu.au
  Subject Code: CSE3OAD
  */
  
import java.lang.reflect.*;
import java.lang.annotation.Annotation;
import java.util.*;

/*The Validation Class below is used to check the annotations of an object that is passed to it via the validate method. It 
  performs this action by a series of logical steps that are outlined as folows: 
  First step: Check to make sure the model that has been passed contains a reference to an object.
  Second Step: Retrieve all the fields of the object that was passed to the method and all the fields of the superclass of that object (if any).
  Third Step: Iterate through each field and check if it has been asigned any annotations.
  Fourth Step: Get the annotation name and the name of the Validator Class.
  Fifth Step: Concatenate these two names together to produce the corresponding validator class of this annotation.
  Sixth Step: Use the name of the concatenated String to get the Class object that is associated with the String that was passed 
  to the Class.forName method. 
  Seventh Step: The validatorClass object is used to make a call to getMethod which takes as an arguement a String that specifies the name of the desired method
  and variable-length of arguments (varargs) of Class objects that are passed to the getMethod and must be placed in a order that reflects the 
  declaretion order of the method that is to be returned.
  Eighth Step: Use the Method object to call the invoke method and pass as an arguement the name of the method that is to be invoked and a varargs of Object types
  that must be the same length of the formal parameter arguement list of the method that is to be returned.
  (Note: The ValidationException message that is apart of the last try catch method this class uses, has been changed to cover a greater range of error cases. */
public abstract class Validator 
{
	public abstract void applyRule(
		Annotation annotation, 
		Object fieldValue, 
		Class<?> fieldType
	) throws Exception;

	public static final String APPLY_RULE_METHOD_NAME = "applyRule";

	private static Map<String, Object> validatorMap = new HashMap<String, Object>();

	// NOTE: this method is static; it could have been placed in any other Class.
	//		 we've put it in this abstract class because it is related to the overall
	//		 validation use case that includes this "Validator" abstract class.
	public static void validate(Object model) throws Exception 
	{
        if (model == null)
            throw new ValidationException(
            	String.join(" ", "Model [", model.getClass().getName(), "] cannot be null")
            );

		List<Field> fields = getInheritedDeclaredFields(model);

		for(Field field: fields) 
		{
			Annotation[] annotations = field.getDeclaredAnnotations();
			
			for(Annotation annotation: annotations) 
			{				
				String annotationName = annotation.annotationType().getName();
				String abstractValidatorClassName = Validator.class.getSimpleName();
				/** 
				 *	some convention over configuration
				 *	convention: 
				 * 		if your annotation is named "Min" and we know this abstract class is
				 * 		named "Validator", then your validator rule will have to be implemented 
				 *		in a file named "MinValidator" ("Min" + "Validator" joined/concatenated)
				 *
				 *	Why are we doing this?
				 *		if we have an annotation "Min", based on that annotation, this Framework
				 *		can then find the matching validator rule in a file named "MinValidator".
				 *		This static "validate" method uses Java Reflection to:
				 *			- the current forEach iteration's annotation (find the name in String)
				 *			- this abstract class name (in String)
				 *			- concat them together, annotation name first
				 *			- looks for a class matching the concatenated String
				 *				> this class should extend abstract class "Validator"
				 *			- create an instance of that class 
				 *				> (through it's constructor, retieved using Java Reflection)
				 *			- calls its "applyRule" method (with appropriate arguments)
				 *				> we know the instance of the class has the "applyRule" method
				 *				> because this class extends abstract class "Validator"
				 *				> and this abstract class "Validator" has an abstract "applyRule" method
				 */
				String validatorClassName = String.join("", annotationName, abstractValidatorClassName);

				Class<?> validatorClass;
				
				try 
				{
					 validatorClass = Class.forName(validatorClassName);
				} 
				catch (Exception exp) 
				{
					throw new ValidationException(
						"Cannot find Validator subclass " + validatorClassName +
						".class to validate targetted field \"" + String.join(".", field.getDeclaringClass().getName(), field.getName()) + 
						"\" annotated with @" + annotationName + ". Was such a subclass defined?"
						);
				}

				if (validatorMap.get(validatorClassName) == null)
					validatorMap.put(validatorClassName, validatorClass.getConstructor().newInstance(new Object[0]));
					Object validatorInstance = validatorMap.get(validatorClassName);
					field.setAccessible(true); //to access private field
				
				Method method = validatorClass.getMethod(
					APPLY_RULE_METHOD_NAME, 
					Annotation.class, 
					Object.class, 
					Class.class
				);

				try 
				{
					method.invoke(validatorInstance, annotation, field.get(model), field.getType());
				} 
				catch (InvocationTargetException ite) 
				{
					/*The ValidationException message has been changed to cover a greater range of error cases.*/
					if (ite.getCause() instanceof ValidationException)
						throw new ValidationException(
							"Field [ " + 
							String.join(".", field.getDeclaringClass().getName(), field.getName()) + 
							" ]" + (field.getName().equalsIgnoreCase("section")? " must have a value of either FREEZER, MEAT, COOLING or CRISPER and " : "") + ite.getCause().getMessage()
						);
				}
			}
		}
	}

	/**
	 * finding fields from model Class, as well as fields from its superclass hierarchy
	 */
	public static List<Field> getInheritedDeclaredFields(Object model) throws Exception 
	{
		List<Field> fields = new ArrayList<Field>();
		
		Class<?> modelClass = model.getClass();
		while (modelClass != null) 
		{
			fields.addAll(Arrays.asList(modelClass.getDeclaredFields()));
			modelClass = modelClass.getSuperclass();
		}

		return fields;
	}

	private final static Set<Class<?>> NUMBER_REFLECTED_PRIMITIVES;
	static 
	{
	    Set<Class<?>> s = new HashSet<>();
	    s.add(byte.class);
	    s.add(short.class);
	    s.add(int.class);
	    s.add(long.class);
	    s.add(float.class);
	    s.add(double.class);
	    NUMBER_REFLECTED_PRIMITIVES = s;
	}

	public static boolean isReflectedAsNumber(Class<?> type) 
	{
	    return Number.class.isAssignableFrom(type) || NUMBER_REFLECTED_PRIMITIVES.contains(type);
	}	
}