/*Name: Marco Aerlic
  Student ID: 20690053
  Username: 20690053@students.ltu.edu.au
  Subject Code: CSE3OAD
  */

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Grocery 
{
    public static final int MINIMUM_QUANTITY = 1;
	
	/*id is automatically allocated and incremented by the grocery table in the fridgedb database, there is no need for any 
	  @Min or @Max value to be added.*/ 
    private int id;

    /*For a Grocery instance to be instantiated there must first be an Item in existence, therefore we have placed the @NotNull annotation for the item attribute.*/ 
	@NotNull
    private Item item;
	
    /*A LocalDate object may be null when creating a Grocery instance either through the POST method or in the main method below, as the program caters too both 
	  scenarios.*/
    private LocalDate date;
    
	/*A minimum value for a quantity of a Grocery must be greater then or equal to one. This is necessary as it makes no sense to have a Grocery with a quantity 
	  of zero, this is impossible.*/
	@Min(value=MINIMUM_QUANTITY)
    private int quantity;
	
	/*A Grocery instance must have a SECTION value in order to exist, therefore the section attribute cannot be null.*/
	@NotNull
    private FridgeDSC.SECTION section;


    public Grocery(int id, Item item, LocalDate date, int quantity, FridgeDSC.SECTION section) 
	{
        this.id = id;
        this.item = item;
        this.date = date != null ? date : LocalDate.now();
        this.quantity = quantity;
        this.section = section;
    }

    public Grocery(Item item, LocalDate date, int quantity, FridgeDSC.SECTION section) throws Exception 
	{
        this(0, item, date, quantity, section);
    }

    public Grocery(Item item, int quantity, FridgeDSC.SECTION section) throws Exception 
	{
        this(item, null, quantity, section);
    }

    public int getId() 
	{
        return this.id;
    }

    public Item getItem() 
	{
        return this.item;
    }

    public String getItemName() 
	{
        return this.item.getName();
    }

    public LocalDate getDate() 
	{
        return this.date;
    }

    public String getDateStr() 
	{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(FridgeDSC.DATE_FORMAT);

        return this.date.format(dtf);
    }

    public String getDaysAgo() 
	{
        return FridgeDSC.calcDaysAgoStr(date);
    }

    public int getQuantity() 
	{
        return this.quantity;
    }

    public void updateQuantity() 
	{
        this.quantity--;
    }

    public FridgeDSC.SECTION getSection() 
	{
        return this.section;
    }
	
    @Override
    public String toString() 
	{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(FridgeDSC.DATE_FORMAT);
        String daysAgo = FridgeDSC.calcDaysAgoStr(date);

        String itemStr = null;
        if (this.item != null)
            itemStr = this.item.getName() + (item.canExpire() ? " (EXP)":"");

        return "[ id: " + this.id
            + ", item: " + itemStr 
            + ", date: " + this.date.format(dtf) + " (" + daysAgo + ")"
            + ", quantity: " + this.quantity
            + ", section: " + this.section
            + " ]";
    }


    public static void main(String [] args) throws Exception 
	{
        Item i = new Item("Beef", true);
		
        Grocery g = new Grocery(i,3, FridgeDSC.SECTION.COOLING);
		
        try 
		{
            Validator.validate(g);
        } 
		catch (ValidationException ve) 
		{
            ve.printStackTrace();
        }
    }   
}
