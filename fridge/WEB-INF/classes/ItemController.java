/*Name: Marco Aerlic
  Student ID: 20690053
  Username: 20690053@students.ltu.edu.au
  Subject Code: CSE3OAD
  */

import java.util.List;

/*Similarly to the GroceryController class, the ItemController acts as a proxy for the FridgeRouterServlet and interacts with the FridgeDSC
  on its behalf. ItemController will establish a connection to the fridgedb database using the fridgeDSC object that is instantiated
  upon calling the constructor of this class. This fridgeDSC instance will then be used to execute certain actions dependant
  on the method the ItemController has been invoked to perform.*/  
public class ItemController 
{
    private FridgeDSC fridgeDSC;

    public ItemController(String dbHost, String dbUserName, String dbPassword) throws Exception 
	{
        fridgeDSC = new FridgeDSC(dbHost, dbUserName, dbPassword);

        try 
		{
            fridgeDSC.connect();
        } 
		catch (Exception e) 
		{
            throw new Exception(e.getMessage());
        }
    }
	
	/*Method get calls the getAllItems of the FridgeDSC class. It takes no arguements and  
	  returns a List of Items from the grocery table of the fridgedb database.*/  
    public List<Item> get() throws Exception 
	{	
        return fridgeDSC.getAllItems();
    }
    
	/*Method getAllExpiredItems calls the getAllExpiredItems of the FridgeDSC class. It takes no arguements and  
	  returns a List of Items from the item table of the fridgedb database that have attribute expires set to true.*/ 
    public List<Item> getAllExpiredItems() throws Exception 
	{
        return fridgeDSC.getAllExpiredItems();
    }

    public static void main(String [] args) throws Exception 
	{

        try 
		{
            ItemController ic = new ItemController("localhost:3306/fridgedb", "marco", "oaduser");
            
            System.out.println(ic.get());
			System.out.println(ic.getAllExpiredItems());
            

        } 
		catch (Exception exp) 
		{
            exp.printStackTrace();
        }

    }
}
