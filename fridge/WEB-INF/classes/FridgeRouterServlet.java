/*Name: Marco Aerlic
  Student ID: 20690053
  Username: 20690053@students.ltu.edu.au
  Subject Code: CSE3OAD
   */

import java.io.*;
import java.lang.reflect.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

/*The FridgeRouterServlet class overides the service method of the HttpServlet. Using reflection it obtains all the necessary
  informtion to create and invoke methods of the GroceryController and ItemController class. This is combined
  with the information received from the request object, properly setting information for the response returned to the client 
  and utilizing methods of the Gson class to convert Java too Json. Once this is accomplished the client is sent a response 
  via the responseObj.*/ 
public class FridgeRouterServlet extends HttpServlet 
{
    public static final String CONTENT_TYPE = "application/json";
    public static final String CHARACTER_ENCODING = "utf-8";

    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static final String HTTP_PUT = "PUT";
    public static final String HTTP_DELETE = "DELETE";

    public static final String CONTROLLER_STR = "Controller";


    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		//ResponseObj used to send data in the form of Json back to the client.
        Object responseObj = null;
		
		//Set the response to the client to be in JSON format, encoded in the UTF-8 character encoding.
		response.setContentType(CONTENT_TYPE);
		response.setCharacterEncoding(CHARACTER_ENCODING);
		
		//Getting the path information from the URL using the request object.
        String pathInfo = request.getPathInfo(); 
		
		//Returns the name of the Http method that was used to make the request, e.g. POST, DELETE, etc.
        String httpMethod = request.getMethod();
		
		/*Getting the path information from the URL using the request object, this will be used to create and instantiate
		  the modelClass and ControllerClass objects. It is also required to check the parameters of the request sent in the URL 
		  from the client especially in the case of GET,PUT and DELETE Http methods where a model id may be sent in a request
		  , e.g. id to be updated, id to be deleted, etc.*/
        String pathInfoArray[] = pathInfo.split("/");
		
        try 
		{
			//Error case if the user has only input the URL information up to the /api or /api/ endpoint.
            if (pathInfoArray.length <= 1) 
			{
                throw new MissingArgumentException("Resource target not defined.");
            }
			
			//Obtaining the modelName from the array pathInfo, will be either grocery or item.
            String modelName = pathInfoArray[1]; 
			
			/*Changing the model name to all lower case, this accounts for the user entering an endpoint of the form 
			  /iteM or /groCery. After this line has been executed the program will run error free should an incident
			  occur like the one described in the previous sentence.*/
            modelName = modelName.toLowerCase();
			
			/*Taking the element at index 0 (so if this modelName contains grocery then the 0th element is just the letter "g"
			  and converting "g" to uppercase "G"). Then join back with the rest of the String it was apart of, e.g. "G" + "rocery".*/
            modelName = modelName.substring(0, 1).toUpperCase() + modelName.substring(1);

			/*Joining the modelName with the final String CONTROLLER_STR. This will be used to obtain a String literal of the form
			ItemController or GroceryController.*/			  
            String controllerName = String.join("", modelName, CONTROLLER_STR);
			
			/*The method Class.forName finds the class with the String name that has been passed to it as an arguement.
			  It then returns the Class object associated with the class that was located with the String that was passed to the method.*/
            Class<?> controllerClass = Class.forName(controllerName); 
			
			/*Similarly to the above Class.forName call, the Class object associated with the class that matches the given String name
			  is returned from calling this method.*/
            Class<?> modelClass = Class.forName(modelName); 
			
			/*Create a String array and uses it to store the values returned from accessing the web.xml files context parameters, this is
			  where the values of the initialization parameters (dbhost, dbusername and dbpassword) with the given names can be obtained.*/
            String[] dbConfig = new String[3];
            dbConfig[0] = getServletContext().getInitParameter("dbhost");
            dbConfig[1] = getServletContext().getInitParameter("dbusername");
            dbConfig[2] = getServletContext().getInitParameter("dbpassword");
            
			/*Returns the Constructor object that matches the specified public constructor of the Class object that is calling the method. 
			  The parameter types are an array of Class objects that must be of the right Class object type and should be 
			  stated in the exact order as they were declared, as they must reflect the formal parameters and the sequence in which those
			  parameters were declared in the constructor that the programmer desires to have returned.*/	  
            Constructor constructor = controllerClass.getConstructor(
                new Class[] {String.class, String.class, String.class}
            );
			
			/*Create and initialize a new instance of the constructors declaring class, this is accomplished by using the constructor 
			  represented by the Constructor object. The initialization parameters must be of type Object, match the formal parameters
			  and the sequence in which those parameters were declared in the definition of the method of the calling Constructor object.*/
            Object controllerInstance = constructor.newInstance((Object[]) dbConfig);

            int modelId = 0;
            Method method = null;
            
            switch (httpMethod) 
			{
                case HTTP_GET:
                    
                    if (pathInfoArray.length >= 3) 
					{
                        if (pathInfoArray[2].equals("e")) 
						{ 
                            method = controllerClass.getMethod("getAllExpiredItems");

                            responseObj = method.invoke(controllerInstance);
                        } 
						else 
						{
							/*Get the id that is placed at the end of the URL by the user, change (parse) it from a String to an Integer.*/
                            modelId = Integer.parseInt(pathInfoArray[2].trim()); 
							
							/*A method object is returned that mirrors the specified method of the calling class. In this case the get method with parameter int is returned.*/
                            method = controllerClass.getMethod("get", int.class);
							
							/*Invoke the method represented by the method object that was created by the controllerClass. 
							  The controllerInstance and the modelId are passed as an arguement to the method invoke and is used to call the get(int id) method.
							  The returned value from calling the method is placed in the responseObj, to be sent back to the client.*/
                            responseObj = method.invoke(controllerInstance, modelId);
								
                            if (responseObj == null) 
							{
                                throw new ResourceNotFoundException(modelName + " with id " + modelId + " not found!");
                            }
                        }
						
                    }
                    else 
					{
						/*In the case the length of pathInfoArray is less than three. This means the user has placed no value at the endpoint of the URl.
						  For example api/grocery/.*/
                        method = controllerClass.getMethod("get");
						
						/*The controllerInstance is passed as an arguement and used to call the get method of the class that the controllerInstance 
						  represents. Note: there were no parameters specified when creating this method object, that is why there is no need to provide
						  an arguement for anything other then the object used to call the method.*/
                        responseObj = method.invoke(controllerInstance);
                    }
					
                    break;
					
                case HTTP_POST: 

                    String resourceData = buildResourceData(request);
			
                    method = controllerClass.getMethod("add", modelClass);

                    Object id = method.invoke(controllerInstance, new Gson().fromJson(resourceData, modelClass));

                    Map<String, String> message = new HashMap<String, String>();
                    message.put("message", "created ok! " + modelName + " id " + Integer.parseInt(id.toString()));
                    responseObj = message;
					
                    break;
					
                case HTTP_PUT:
				
                    if (pathInfoArray.length >= 3) 
					{
						/*In the case that pathInfoArray is greater than equal to three, get what the user has placed at the endpoint of the URL. 
			              Then parse what is retrieved from the endpoint, from a String to an Integer.*/
                        modelId = Integer.parseInt(pathInfoArray[2].trim()); 
                        
						/*The Class object is used to get the method with the specified signature of the class that is associated
						  with the object that called the method. A method object is returned from this invocation.*/
                        method = controllerClass.getMethod("update", int.class); 
					    
						/*The invoke method uses the arguements passed to it to call the the underyling method represented by this method object.*/ 
                        responseObj = method.invoke(controllerInstance, modelId); 
					
                        if (responseObj == null)
						{
                            throw new ResourceNotFoundException(modelName + " with id " + modelId + " not found! Cannot Update!");
						}
                    } 
					else
					{
                        throw new MissingArgumentException("Attribute id required! Cannot Update!");
					}
					
                    break;
					
                case HTTP_DELETE:
				
                    if (pathInfoArray.length >= 3) 
					{
						/*Parse the id that the user has placed at the endpoint of the URL. */
                        modelId = Integer.parseInt(pathInfoArray[2].trim()); 
						
						/*Returns a method object that mirrors the delete method of the class represented by this controllerClass object.*/
                        method = controllerClass.getMethod("delete", int.class);
						
						/*Invoke the method associated with this method object, using the two arguements passed (controllerInstance and modelId).*/
                        responseObj = method.invoke(controllerInstance, modelId); 
						
						//Check if no rows were affected, in the case that a value equal to or less than zero is returned throw a ResourceNotFoundException.
                        if (Integer.parseInt(responseObj.toString()) <= 0)
						{
                            throw new ResourceNotFoundException(modelName + " with id " + modelId + " not found! Cannot Delete!");
						}
						else
						{
							responseObj = buildMessage(modelName + " with id " + modelId + " deleted!");
						}
                    } 
					else
					{
                        throw new MissingArgumentException("Attribute id required! Cannot Delete!");
					}
					
                    break;
					
                default:
                  
                    throw new NoSuchMethodException();
            } 

            response.getWriter().print(new Gson().toJson(responseObj));
        }
        catch (Exception exp) 
		{	
			//Set the String message, to exception message.	
            String message = exp.getMessage();
			        
            response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);		
			
            if (exp instanceof ResourceNotFoundException)
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);

            if (exp instanceof MissingArgumentException)
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);

            if (exp instanceof ClassNotFoundException) 
			{
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                message = "Resource not found!";
            }

            if (exp instanceof NoSuchMethodException) 
			{
                response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                message = "Method not allowed on specified resource!";
            }
			
			
            if (exp instanceof InvocationTargetException) 
			{  
                /*Use the getCause method of the Throwable class to determine the method that threw the exception.
				  UpdateNotAllowedException, this exception occurs when issuing a PUT request and the value of the Grocery quantity is equal to one.*/		
				if (exp.getCause() instanceof UpdateNotAllowedException)
				{
					/*Set the status code for this response.*/
					response.setStatus(HttpServletResponse.SC_FORBIDDEN);  
					
					//Get the message from the exception that threw the error.
					message = exp.getCause().getMessage();              
				}
				
				/*ValidationException, this exception occurs when when issuing a POST request and the Grocery quantity is less than the minimum
				  value specified by the documentation for this program. (Note: See, Client Programming Documentation: [key:value table] for minimum value allowed 
				  for the quantiity attribute of a Grocery).*/
				if (exp.getCause() instanceof ValidationException)
				{
					/*Set the status code for this response.*/
					response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
					
					//Get the message from the exception that threw the error.
					message = exp.getCause().getMessage();
				}
				
				/*This exception occurs when attempting to create a new Grocery with a null Item name.*/
				if (exp.getCause() instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException)
				{
					/*Set the status code for this response.*/
					response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
					
					//Custom error message.
					message = "Error: The Item instance of the Grocery you are trying add cannot be null. Exception thrown: " + exp.getCause() + "";

				}
            }
			
			/*NumberFormatException, this exception occurs when issuing a GET request for a specific Grocery and the
			value of the endpoint grocery/<somevalue> is neither e nor an id of a grocery that is currently in the 
			database, item/<somevalue> will issue the same error when searching for expired items and the endpoint is
			any other value besides e.*/
			if (exp instanceof NumberFormatException)
			{
				/*Set the status code for this response.*/
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);	
				
			    //Custom error message.
				message = "Error: You must enter meaningful data";
			}
			
			/*com.google.gson.JsonSyntaxException, occurs when issuing a POST request in combination with this HTTP error 
			  when there are syntax errors in the Json String you are trying to add as a Grocery instance, e.g. missing brackets, attributes that
			  have not been given a value, etc.*/
			if (exp instanceof com.google.gson.JsonSyntaxException)
			{
				/*Set the status code for this response.*/
				response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
				
			    //Custom error message.
				message = "Error: Check for incorrect syntax specifications in the Json string you are trying to add as a Grocery instance. Exception thrown: " + exp.getMessage() + "";
			}
			
			/*NullPointerException, occurs when issuing a POST request and providing no data.*/
			if (exp.getCause() instanceof NullPointerException)
			{
				/*Set the status code for this response.*/
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				message = "Error: In a POST request you must supply appropriate data to add a Grocery to the fridgedb database. Exception thrown: " + exp.getCause();
			}
			
            response.getWriter().write(new Gson().toJson(buildMessage(message)));
        }

    }

    private String buildResourceData(HttpServletRequest request) throws Exception 
	{
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = request.getReader();
		
        try 
		{
            String line;
			
            while ((line = reader.readLine()) != null) 
			{
                sb.append(line).append('\n');
            }
        } 
		finally 
		{
            reader.close();
        }
		
        return sb.toString();
    }

    private Map buildMessage(String msg) 
	{
        Map<String, String> message = new HashMap<String, String>();
        message.put("message", msg);
        return message;
    }

}
