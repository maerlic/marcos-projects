/*Name: Marco Aerlic
  Student ID: 20690053
  Username: 20690053@students.ltu.edu.au
  Subject Code: CSE3OAD
  */

import java.sql.*;
import java.util.*;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import java.time.LocalDate;
import java.time.Duration;
import java.time.format.DateTimeFormatter;

public class FridgeDSC 
{
    public static final String DATE_FORMAT = "dd/MM/yyyy";

    public enum SECTION 
	{
        FREEZER,
        MEAT,
        COOLING,
        CRISPER
    };

    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;

    private String dbUserName;
    private String dbPassword;
    private String dbURL;
        
    public FridgeDSC(String dbHost, String dbUserName, String dbPassword) 
	{
        this.dbUserName = dbUserName;
        this.dbPassword = dbPassword;
        this.dbURL = "jdbc:mysql://" + dbHost;
    }

    public void connect() throws SQLException 
	{
        try 
		{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(dbURL, dbUserName, dbPassword);
            statement = connection.createStatement();
        } 
		catch(Exception e) 
		{
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public void disconnect() throws SQLException 
	{
        if(preparedStatement != null) preparedStatement.close();
        if(statement != null) statement.close();
        if(connection != null) connection.close();
    }



    public Item searchItem(String name) throws Exception 
	{
        String queryString = "SELECT * FROM item WHERE name = ?";
        preparedStatement = connection.prepareStatement(queryString);
        preparedStatement.setString(1, name);
        ResultSet rs = preparedStatement.executeQuery();

        Item item = null;

        if (rs.next()) 
		{ 
            boolean expires = rs.getBoolean(2);
            item = new Item(name, expires);
        }

        return item;
    }

    public Grocery searchGrocery(int id) throws Exception 
	{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);
        String queryString = "SELECT * FROM grocery WHERE id = ?";
        preparedStatement = connection.prepareStatement(queryString);
        preparedStatement.setInt(1, id);
        ResultSet rs = preparedStatement.executeQuery();

        Grocery grocery = null;

        if (rs.next()) 
		{ 
            String itemName = rs.getString(2);
            Item item = searchItem(itemName);
            if (item == null) {
                System.err.println("[WARNING] Item: '" + itemName + "'' does not exist!");
            }
            LocalDate date = LocalDate.parse(rs.getString(3), dtf);
            int quantity = rs.getInt(4);
            FridgeDSC.SECTION section = SECTION.valueOf(rs.getString(5));

            grocery = new Grocery(id, item, date, quantity, section);

        }

        return grocery;
    }

    public List<Item> getAllItems() throws Exception 
	{
        String queryString = "SELECT * FROM item";
        ResultSet rs = statement.executeQuery(queryString);

        List<Item> items = new ArrayList<Item>();

        while (rs.next()) 
		{ 
            String name = rs.getString(1);
            boolean expires = rs.getBoolean(2);
            items.add(new Item(name, expires));
        }

        return items;
    }

    public List<Item> getAllExpiredItems() throws Exception 
	{
        String queryString = "SELECT * FROM item where expires is true";
        ResultSet rs = statement.executeQuery(queryString);

        List<Item> items = new ArrayList<Item>();

        while (rs.next()) 
		{ 
            String name = rs.getString(1);
            boolean expires = rs.getBoolean(2);
            items.add(new Item(name, expires));
        }

        return items;
    }   


    public List<Grocery> getAllGroceries() throws Exception 
	{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);
        String queryString = "SELECT * FROM grocery";
        ResultSet rs = statement.executeQuery(queryString);

        List<Grocery> groceries = new ArrayList<Grocery>();

        while (rs.next()) 
		{ 
            int id = rs.getInt(1);
            String itemName = rs.getString(2);
            Item item = searchItem(itemName);
            if (item == null) 
			{
                System.err.println("[WARNING] Item: '" + itemName + "'' does not exist!");
                continue;
            }
            LocalDate date = LocalDate.parse(rs.getString(3), dtf);
            int quantity = rs.getInt(4);
            SECTION section = SECTION.valueOf(rs.getString(5));

            groceries.add(new Grocery(id, item, date, quantity, section));
        }

        return groceries;
    }

    public List<Grocery> getAllGroceryExpiredItems() throws Exception 
	{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);
        String queryString = "select * from grocery,item where grocery.itemName=item.name AND item.expires=true";
        ResultSet rs = statement.executeQuery(queryString);

        List<Grocery> groceries = new ArrayList<Grocery>();

        while (rs.next()) 
		{ 
            int id = rs.getInt(1);
            String itemName = rs.getString(2);
            Item item = searchItem(itemName);
            if (item == null) 
			{
                System.err.println("[WARNING] Item: '" + itemName + "'' does not exist!");
                continue;
            }
			
            LocalDate date = LocalDate.parse(rs.getString(3), dtf);
            int quantity = rs.getInt(4);
            SECTION section = SECTION.valueOf(rs.getString(5));

            groceries.add(new Grocery(id, item, date, quantity, section));
        }

        return groceries;
    }



    public int addGrocery(String name, int quantity, SECTION section) throws Exception 
	{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);
        LocalDate date = LocalDate.now();
        String dateStr = date.format(dtf);

        String command = "INSERT INTO grocery VALUES(?, ?, ?, ?, ?)";
        preparedStatement = connection.prepareStatement(command);

        preparedStatement.setInt(1, 0);
        preparedStatement.setString(2, name);
        preparedStatement.setString(3, dateStr);
        preparedStatement.setInt(4, quantity);
        preparedStatement.setString(5, section.toString());

        preparedStatement.executeUpdate();

        ResultSet rs = statement.executeQuery("SELECT LAST_INSERT_ID()");
        rs.next();
        int newId = rs.getInt(1);

        return newId;
    }

    public Grocery useGrocery(int id) throws Exception 
	{
        Grocery g = searchGrocery(id);

        if (g == null)
            return null;

        if (g.getQuantity() <= 1)
            throw new UpdateNotAllowedException("There is only one: " + g.getItemName() + " (bought on " + g.getDateStr() + ") - use DELETE instead.");

        String queryString =
            "UPDATE grocery " +
            "SET quantity = quantity - 1 " +
            "WHERE quantity > 1 " +
            "AND id = " + id + ";";

        if (statement.executeUpdate(queryString) > 0)
            return searchGrocery(id);
        else return null;
    }

    public int removeGrocery(int id) throws Exception 
	{
        String queryString = "SELECT COUNT(*) FROM grocery WHERE id = ?";
        preparedStatement = connection.prepareStatement(queryString);
        preparedStatement.setInt(1, id);
        ResultSet rs = preparedStatement.executeQuery();

        boolean pre = rs.next();
        if(!pre) 
		{ 
            throw new RuntimeException("The grocery does not exist!");
        }

        return statement.executeUpdate("DELETE FROM grocery WHERE id = " + id);
    }

    public static long calcDaysAgo(LocalDate date) 
	{
        return Math.abs(Duration.between(LocalDate.now().atStartOfDay(), date.atStartOfDay()).toDays());
    }

    public static String calcDaysAgoStr(LocalDate date) 
	{
        String formattedDaysAgo;
        long diff = calcDaysAgo(date);

        if (diff == 0)
            formattedDaysAgo = "today";
        else if (diff == 1)
            formattedDaysAgo = "yesterday";
        else formattedDaysAgo = diff + " days ago";

        return formattedDaysAgo;
    }


    public static void main(String[] args) throws Exception 
	{
        FridgeDSC dsc = new FridgeDSC("localhost:3306/fridgedb", "marco", "oaduser");
        try 
		{
            dsc.connect();
            System.out.println(dsc.getAllGroceries());
            System.out.println("USE GROCERY 1" + dsc.removeGrocery(35));
            System.out.println("USE GROCERY 2" + dsc.useGrocery(65));
            System.out.println("USE GROCERY 3" + dsc.useGrocery(6));
        } 
		catch (Exception exp) 
		{
            exp.printStackTrace();
        }
    }
}
