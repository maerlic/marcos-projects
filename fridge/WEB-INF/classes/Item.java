public class Item 
{
	private String name;
	
	private boolean expires; 

	public Item(String name, boolean expires) 
	{
		this.name = name;
		this.expires = expires;
	}

	public Item(String name) 
	{
		this(name, false);
	}

	public String getName() 
	{
		return this.name;
	}

	public boolean canExpire()
	{
		return this.expires;
	}

	public String toString() 
	{
		return "[ name: " + this.name
			+ ", expires: " + this.expires
			+ " ]";
	}

	public static void main(String [] args)
	{
		Item i1 = new Item("Milk", false);
		System.out.println(i1);

		Item i2 = new Item("Fish", true);
		System.out.println(i2);
	}
}