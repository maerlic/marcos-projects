/*Name: Marco Aerlic
  Student ID: 20690053
  Username: 20690053@students.ltu.edu.au
  Subject Code: CSE3OAD
  */

import java.util.List;

/*This class acts as a proxy for the FridgeRouterServlet class amd interacts with the FridgeDSC
  class on its behalf. GroceryController will establish a connection to the fridgedb database using the fridgeDSC object that is instantiated
  upon calling the constructor of this class. This fridgeDSC instance will then be used to execute certain actions dependant
  on the method the GroceryController has been invoked to perform.*/  
public class GroceryController 
{
    private FridgeDSC fridgeDSC;
	
    public GroceryController(String dbHost, String dbUserName, String dbPassword) throws Exception 
	{
        fridgeDSC = new FridgeDSC(dbHost, dbUserName, dbPassword);
		
        try 
		{
            fridgeDSC.connect();
			
        } 
		catch (Exception e) 
		{
            throw new Exception(e.getMessage());
        }
    }
	
	/*Method get calls the getAllGroceries method of the FridgeDSC class. It takes no arguements and  
	  returns a List of Groceries from the grocery table of the fridgedb database.*/	
    public List<Grocery> get() throws Exception 
	{ 
        return fridgeDSC.getAllGroceries(); 
    }
	
	/*Method get with parameter int, calls the searchGrocery method of FridgeDSC class. It takes an int that represents 
	  the id of a grocery as an arguement and passes it to the searchGrocery method. This method will
	  return a grocery instance.*/
    public Grocery get(int id) throws Exception 
	{
        return fridgeDSC.searchGrocery(id); 
    }
	
	/*Method getAllExpiredItems will call the getAllGroceryExpiredItems of the FridgeDSC class. It takes no arguements
	  and returns a List of all the Grocery items with the Item attribute "expires" set too true.*/	
    public List<Grocery> getAllExpiredItems() throws Exception 
	{
        return fridgeDSC.getAllGroceryExpiredItems(); 
    }
	
	/*Method add will call the addGrocery method of the FridgeDSC class. It takes a Gocery instance as an arguement,
	  it then passes this Grocery to the Validator method validate which checks the constraints placed upon  
	  the attributes of the Grocery class, by the annotation interfaces @Min and @NotNull. If all the constraints hold true 
	  the addGrocery method will be called and the item name, quantity and section values of the Grocery instance will 
	  be passed as arguements. The id of the Grocery that has been inserted into the grocery table of the fridgedb database
	  will then be returned from the method.*/	
    public int add(Grocery g) throws Exception 
	{
    	Validator.validate(g);
		 
        return fridgeDSC.addGrocery(g.getItemName(), g.getQuantity(), g.getSection());
    }
	
	/*Method update will call the useGrocery method of the FridgeDSC class. It takes an int that represents the id
	  of a Grocery instance as an arguement and passes it to the useGrocery method of the fridgeDSC class. The method
	  useGrocery will then return a Grocery that represents the updated Grocery instance.*/ 	  
    public Grocery update(int id) throws Exception 
	{
        return fridgeDSC.useGrocery(id);
    }
	
	/*Method delete will call the removeGrocery method of the FridgeDSC class. It takes an int that represents the id
	  of a Grocery instance as an arguement and passes it to the removeGrocery method of the FridgeDSC class. The method removeGrocery will
	  then return an int that represents the number of rows effected by the query that was run to delete the Grocery.*/ 
    public int delete(int id) throws Exception 
	{
        return fridgeDSC.removeGrocery(id); 
    }
	
    public static void main(String [] args) throws Exception 
	{
        System.out.println("GroceryController main...");
        
        try 
		{
            GroceryController gc = new GroceryController("localhost:3306/fridgedb", "marco", "oaduser");
            System.out.println(gc.get()); 
            System.out.println(gc.get(19)); 
            Item myitem = new Item("",true);
            System.out.println(gc.add(new Grocery(myitem,5, FridgeDSC.SECTION.valueOf("MEAT"))));
            System.out.println(gc.update(59)); 
            System.out.println(gc.delete(19));

        } 
		catch (Exception exp) 
		{
            exp.printStackTrace();
        }
        
    }
}
